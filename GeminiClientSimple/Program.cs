﻿// See https://aka.ms/new-console-template for more information

using System;

namespace GeminiClientSimple;

public static class Program
{
    public static void Main(string[] args)
    {
        if (args.Length < 1)
        {
            Console.WriteLine("Please provide url");
            return;
        }

        new GeminiClient().GetPage(args[0]);
    }
}
