using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace GeminiClientSimple;

public class GeminiClient
{
    private readonly X509Store _certStore;
    private readonly X509CertificateCollection _collection;

    public GeminiClient()
    {
        _certStore = new X509Store(StoreLocation.CurrentUser);
        _collection = new X509CertificateCollection();
    }

    public void GetPage(string url)
    {
        var (hostUrl, page) = PrepareUrl(url);
        var client = new TcpClient(hostUrl, 1965);

        using (SslStream sslStream = new SslStream(client.GetStream(), false,
                   ValidateCertificate, null))
        {
            sslStream.AuthenticateAsClient(hostUrl);
            byte[] messageToSend = Encoding.UTF8.GetBytes($"gemini://{hostUrl}{page}\r\n");
            sslStream.Write(messageToSend);

            string responseData = ReadResponse(sslStream);

            var lines = responseData.Split('\n');
            HandleResponse(lines);
        }
    }

    private string ReadResponse(SslStream sslStream)
    {
        byte[] buffer = new byte[2048];
        var messageData = new StringBuilder();
        int bytesRead = -1;
        do
        {
            bytesRead = sslStream.Read(buffer, 0, buffer.Length);
            
            var decoder = Encoding.UTF8.GetDecoder();
            char[] chars = new char[decoder.GetCharCount(buffer, 0, bytesRead)];
            decoder.GetChars(buffer, 0, bytesRead, chars, 0);
            messageData.Append(chars);
        } while (bytesRead != 0);

        return messageData.ToString();
    }

    private void HandleResponse(string[] responseLines)
    {
        var meta = responseLines[0].Split(' ');
        var (responseCode, otherInfo) = (meta[0], meta[1]);

        if (!otherInfo.StartsWith("text/gemini") && responseCode[0] != '3') throw new ArgumentException("Only text/gemini media type is supported");

        switch (responseCode[0])
        {
            case '2':
                PrintContent(responseLines);
                break;
            case '3':
                GetPage(otherInfo);
                break;
            case '4':
                Console.WriteLine("Error: Temporary failure");
                break;
            case '5':
                Console.WriteLine("Error: Permanent failure");
                break;
            default:
                throw new ArgumentException("Unknown response code");
        }
    }
    

    private void PrintContent(string[] lines)
    {
        for (var i = 1; i < lines.Length; i++)
        {
            Console.WriteLine(lines[i]);
        }
    }

    private (string, string) PrepareUrl(string url)
    {
        if (url.StartsWith("gemini://"))
        {
            var u = url.Trim().Substring(9);
            int firstSlashIndex = u.IndexOf('/');
            var server = (firstSlashIndex == -1) ? u : u.Remove(firstSlashIndex);
            var page = (firstSlashIndex == -1) ? "/" : u.Substring(firstSlashIndex, u.Length - firstSlashIndex);
            return (server, page);
        }

        throw new ArgumentException("Unknown url format");
    }

    private bool ValidateCertificate(object sender, X509Certificate certificate,
        X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        if ((sslPolicyErrors.HasFlag(SslPolicyErrors.RemoteCertificateNotAvailable)))
        {
            ConsoleHelper.WriteWarning("WARNING! Certificate not available"+Environment.NewLine);
        }
        
        if ((sslPolicyErrors.HasFlag(SslPolicyErrors.RemoteCertificateNameMismatch)))
        {
            ConsoleHelper.WriteWarning("WARNING! Certificate name mismatch"+Environment.NewLine);
        }
        
        return true;
    }
}